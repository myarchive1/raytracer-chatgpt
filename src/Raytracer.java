import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Raytracer {
    public static void main(String[] args) {
        int width = 1980;
        int height = 1080;

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        double aspectRatio = (double) width / height;
        double fov = Math.PI / 2;
        double angle = Math.tan(fov / 2);

        Vector3D origin = new Vector3D(0, 0, 0);
        Sphere[] spheres = {
            new Sphere(new Vector3D(0, 0, -5), 1),
            new Sphere(new Vector3D(1, 0, -5), 0.5),
            new Sphere(new Vector3D(-1, 0, -5), 0.5)
        };

        // Adjust the light source position
        Vector3D lightDirection = new Vector3D(1, 1, -2).normalize(); // Light source slightly farther away

        BufferedImage skyboxImage = null;
        try {
            skyboxImage = ImageIO.read(new File("skybox.png"));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                double xx = (2 * ((x + 0.5) / width) - 1) * angle * aspectRatio;
                double yy = (1 - 2 * ((y + 0.5) / height)) * angle;

                Vector3D direction = new Vector3D(xx, yy, -1).normalize();

                Color color = traceRay(origin, direction, spheres, lightDirection, skyboxImage, width, height, x, y, 3);

                image.setRGB(x, y, color.getRGB());
            }
        }

        try {
            File output = new File("output.png");
            ImageIO.write(image, "png", output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Color traceRay(Vector3D origin, Vector3D direction, Sphere[] spheres, Vector3D lightDirection, BufferedImage skyboxImage, int screenWidth, int screenHeight, int x, int y, int depth) {
        if (depth == 0) {
            return new Color(0, 0, 0, 0); // Limit the recursion depth to avoid infinite loops
        }

        double minDistance = Double.POSITIVE_INFINITY;
        Sphere hitSphere = null;

        for (Sphere sphere : spheres) {
            double distance = sphere.intersect(origin, direction);
            if (distance > 0 && distance < minDistance) {
                minDistance = distance;
                hitSphere = sphere;
            }
        }

        if (hitSphere != null) {
            Vector3D intersectionPoint = origin.add(direction.scale(minDistance));
            Vector3D normal = intersectionPoint.subtract(hitSphere.center).normalize();
            double intensity = normal.dot(lightDirection);

            // Add shadow effect
            double shadowIntensity = 0.3; // Reduce shadow intensity
            if (intensity < 0) {
                intensity = 0;
            } else {
                // Calculate the angle between the normal and light direction
                double angle = Math.acos(normal.dot(lightDirection));
                // Adjust intensity based on angle (more shadow on the left side)
                intensity *= 1 - shadowIntensity * Math.sin(angle);
                // Ensure intensity remains within the valid range
                intensity = Math.max(intensity, 0);
            }

            // Increase red color brightness
            intensity = Math.min(intensity * 2.0, 1.0); // Increase the brightness by 100%

            int red = (int) (intensity * 255);
            return new Color(red, 0, 0);
        } else {
            // Check for intersection with transparent circle
            Sphere transparentSphere = new Sphere(new Vector3D(1, 1, -5), 1.0); // Larger radius
            double transparentDistance = transparentSphere.intersect(origin, direction);
            if (transparentDistance > 0) {
                // Cast reflection rays
                Vector3D intersectionPoint = origin.add(direction.scale(transparentDistance));
                Vector3D normal = intersectionPoint.subtract(transparentSphere.center).normalize();
                Vector3D reflectionDirection = direction.subtract(normal.scale(2 * direction.dot(normal))).normalize();

                // Trace reflection ray with increased depth
                Color reflectionColor = traceRay(intersectionPoint, reflectionDirection, spheres, lightDirection, skyboxImage, screenWidth, screenHeight, x, y, depth - 1);

                // Apply reflection color to the transparent circle
                return reflectionColor;
            } else {
                // Map the skybox image to the background
                int bx = (int) (x / (double) screenWidth * skyboxImage.getWidth());
                int by = (int) (y / (double) screenHeight * skyboxImage.getHeight());
                return new Color(skyboxImage.getRGB(bx, by));
            }
        }
    }
}

class Vector3D {
    double x, y, z;

    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3D add(Vector3D v) {
        return new Vector3D(x + v.x, y + v.y, z + v.z);
    }

    public Vector3D subtract(Vector3D v) {
        return new Vector3D(x - v.x, y - v.y, z - v.z);
    }

    public Vector3D scale(double s) {
        return new Vector3D(x * s, y * s, z * s);
    }

    public double dot(Vector3D v) {
        return x * v.x + y * v.y + z * v.z;
    }

    public double length() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    public Vector3D normalize() {
        double length = length();
        return new Vector3D(x / length, y / length, z / length);
    }
}

class Sphere {
    Vector3D center;
    double radius;

    public Sphere(Vector3D center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    public double intersect(Vector3D origin, Vector3D direction) {
        Vector3D oc = origin.subtract(center);
        double a = direction.dot(direction);
        double b = 2 * oc.dot(direction);
        double c = oc.dot(oc) - radius * radius;
        double discriminant = b * b - 4 * a * c;

        if (discriminant < 0) {
            return -1; // No intersection
        } else {
            double t1 = (-b + Math.sqrt(discriminant)) / (2 * a);
            double t2 = (-b - Math.sqrt(discriminant)) / (2 * a);
            if (t1 > 0 && t2 > 0) {
                return Math.min(t1, t2); // Closest intersection
            } else if (t1 > 0) {
                return t1;
            } else if (t2 > 0) {
                return t2;
            } else {
                return -1; // Both intersections are behind the ray
            }
        }
    }
}
